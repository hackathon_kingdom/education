import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/parents',
      name: 'parents',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "parents" */ './views/Parents.vue')
    },
    {
      path: '/parents/investing',
      name: 'parents.investing',
      component: () => import(/* webpackChunkName: "parents" */ './views/Investing.vue')
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "parents" */ './views/About.vue')
    },
    {
      path: '/kids',
      name: 'kids',
      component: () => import(/* webpackChunkName: "kids" */ './views/Kids.vue')
    },
    {
      path: '/games/donate',
      name: 'games.donate',
      component: () => import(/* webpackChunkName: "games.donate" */ './views/DonateGame.vue')
    },
    {
      path: '/games/hunger',
      name: 'games.hunger',
      component: () => import(/* webpackChunkName: "games.donate" */ './views/HungerGame.vue')
    },
  ]
})
